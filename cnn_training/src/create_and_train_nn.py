import os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import time
import random
from tensorflow.keras import regularizers
from PIL import Image
import image_config
## SETUP ##
# define constants
AUTOTUNE = tf.data.experimental.AUTOTUNE

IMG_SHAPE = (image_config.IMG_WIDTH, image_config.IMG_HEIGHT, image_config.CHANNELS)
# same as in cats vs dogs
BATCH_SIZE = 2

default_timeit_steps = 1000


def show_images_of_path(image_paths):
    for image_path in image_paths:
        # dont know what display should do
        # display.display(Image.open(str(image_path)).show())
        plt.imshow(Image.open(str(image_path)))
        plt.show()


def augment_image(image_path):
    label = get_label(image_path)
    image_string = tf.io.read_file(image_path)
    img = image_config.decode_img(image_string)
    if random.randint(0, 1):
        img = tf.image.flip_left_right(img)
    img = tf.image.random_brightness(img, max_delta=0.5)
    img = tf.image.random_saturation(img, 0, 3)
    return img, label


def get_label(file_path):
    dirs = tf.strings.split(file_path, os.path.sep)
    return dirs[-2] == CLASS_NAMES


def process_path(file_path):
    label = get_label(file_path)
    # load raw data as string
    img = tf.io.read_file(file_path)
    img = image_config.decode_img(img)
    return img, label


def prepare_for_training(data_set, cache=False, shuffle_buffer_size=1000):
    # normal shuffle buffer 1000 but this needs a decade
    # prepare dataset for traing (shuffle and  batch)

    # This is a small dataset, only load it once, and keep it in memory.
    # use `.cache(filename)` to cache preprocessing work for datasets that don't
    # fit in memory.
    if cache:
        if isinstance(cache, str):
            data_set = data_set.cache(cache)
        else:
            data_set = data_set.cache()

    data_set = data_set.shuffle(buffer_size=shuffle_buffer_size)
    # repeat forever
    data_set = data_set.repeat()

    data_set = data_set.batch(BATCH_SIZE)

    # prefetch lets fetch dataset batches in background while model is training
    data_set = data_set.prefetch(buffer_size=AUTOTUNE)

    return data_set


def timeit(dataset, steps=default_timeit_steps):
    # function to check performance of dataset
    start = time.time()
    it = iter(dataset)
    for i in range(steps):
        batch = next(it)
        if i % 10 == 0:
            print('.', end='')
    print()
    end = time.time()

    duration = end - start
    print("{} batches: {} s".format(steps, duration))
    print("{:0.5f} Images/s".format((BATCH_SIZE * steps / duration)))


def visualize(original, augmented):
    fig = plt.figure()
    plt.subplot(1, 2, 1)
    plt.title('Original image')
    plt.imshow(original)

    plt.subplot(1, 2, 2)
    plt.title('Augmented image')
    plt.imshow(augmented)
    plt.show()


def make_model(dropout=False, l2=False):
    # create model from moblieNetV2 with pretrained weigts (imagenet)
    # exlude classifier (top layer)
    base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                                   include_top=False,
                                                   weights='imagenet')

    image_batch, label_batch = next(iter(train_dataset))
    feature_batch = base_model(image_batch)

    # freeze weights -> just train classifier
    base_model.trainable = False
    base_model.summary()

    # last layer of pooling and feature extraction will be trained.
    global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
    feature_batch_average = global_average_layer(feature_batch)

    # add predictio layer
    if l2:
        prediction_layer = tf.keras.layers.Dense(2, kernel_regularizer=regularizers.l2(0.1))
    else:
        prediction_layer = tf.keras.layers.Dense(2)
    prediction_batch = prediction_layer(feature_batch_average)
    print(prediction_batch.shape)
    if dropout:
        model = tf.keras.Sequential([
            base_model,
            global_average_layer,
            prediction_layer,
            tf.keras.layers.Dropout(0.3),
        ])
    else:
        model = tf.keras.Sequential([
            base_model,
            global_average_layer,
            prediction_layer,
        ])
    model.summary()
    # compile model before training
    # cross-etropy and from_logits true beacuse of linear output
    # RMSprop ist an optimizer vor Gradient desenent 0.0001 is the default learning rate
    base_learning_rate = 0.0001
    model.compile(optimizer=tf.keras.optimizers.RMSprop(learning_rate=base_learning_rate),
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    return model


def plot_history(history, modelname):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label=modelname + ' Training Accuracy')
    plt.plot(val_acc, label=modelname + ' Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy ' + modelname)

    plt.subplot(2, 1, 2)
    plt.plot(loss, label=modelname + ' Training Loss')
    plt.plot(val_loss, label=modelname + ' Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss ' + modelname)
    plt.xlabel('epoch')


def train_model(model, dataset, initial_epochs=10, init_steps=43):
    loss0, accuracy0 = model.evaluate(test_dataset, steps=init_steps)
    print("initial loss: {:.2f}".format(loss0))
    print("initial accuracy: {:.2f}".format(accuracy0))

    history = model.fit(dataset,
                        epochs=6,
                        validation_data=test_dataset,
                        steps_per_epoch=371,
                        validation_steps=43

                        )
    return history


def create_dataset(augmented, data_dir, cache_path):
    files = tf.data.Dataset.list_files(
        str(data_dir / '*/*'))
    # get classes out od dir names
    if augmented:
        dataset = files.map(augment_image, num_parallel_calls=AUTOTUNE)
    else:
        dataset = files.map(process_path, num_parallel_calls=AUTOTUNE)
    return prepare_for_training(dataset, cache_path)


train_data_dir = pathlib.Path('../dataset/train_data')
image_count = len(list(train_data_dir.glob('*/*.jpeg')))
print("count train images: ", image_count)


test_data_dir = pathlib.Path('../dataset/validation_data')
image_count = len(list(test_data_dir.glob('*/*.jpeg')))
print("count val images: ", image_count)

CLASS_NAMES = np.array([item.name for item in train_data_dir.glob('*')])
print("Class name: ", CLASS_NAMES)
# create datasets for training
augmented_train_dataset = create_dataset(True, train_data_dir, "./augmented.passata.tfcache")
train_dataset = create_dataset(False, train_data_dir, "./passata.tfcache")
test_dataset = create_dataset(False, test_data_dir, "./test_passata.tfcache")



### start with transferlearning
#standard_model = make_model()
#augmented_model = make_model()
dropout_model = make_model(True)
#l2_model = make_model(False, True)
#l2_dropout_model = make_model(True, True)
#aug_dropout_model = make_model()
#aug_l2_dropout = make_model(True, True)

#standard_history = train_model(standard_model, train_dataset)
#augmented_history = train_model(augmented_model, augmented_train_dataset)
dropout_history = train_model(dropout_model, train_dataset)
#l2_history = train_model(l2_model, train_dataset)
#l2_dropout_history = train_model(l2_dropout_model, train_dataset)
#aug_dropout_history = train_model(aug_dropout_model, augmented_train_dataset)
#aug_l2_dropout_history = train_model(aug_l2_dropout, augmented_train_dataset)

#plot_history(standard_history, "std-model")
#plot_history(augmented_history, "aug_model")
plot_history(dropout_history, "drop_model")
#plot_history(l2_history, "l2_model")
#plot_history(l2_dropout_history, "l2_dropout_model")
#plot_history(aug_dropout_history, "aug_dropout")
#plot_history(aug_l2_dropout_history, "aug_l2_drop")

plt.show()

# add sonftmax layer for propability during classification
pmodel = tf.keras.Sequential([dropout_model,
                                       tf.keras.layers.Softmax()])
pmodel.save('detect_glass_container_cnn')

converter = tf.lite.TFLiteConverter.from_keras_model(pmodel)
tflite_model = converter.convert()

with tf.io.gfile.GFile('model.tflite', 'wb') as f:
  f.write(tflite_model)
