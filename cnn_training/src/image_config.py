import tensorflow as tf
import os


CHANNELS = 3
IMG_HEIGHT = 96
IMG_WIDTH = 96


def decode_img(img):
    # convert compressed string to a 3D uint8 tensor
    img = tf.image.decode_jpeg(img, channels=CHANNELS)
    # convert to bloats betwwen 0 and 1
    img = tf.image.convert_image_dtype(img, tf.float32)
    return tf.image.resize(img, [IMG_WIDTH, IMG_HEIGHT])