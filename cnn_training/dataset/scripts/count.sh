#!/bin/bash
COUNT_VALIDATION_IMAGES=$(find ../validation_data -type f | wc -l)
COUNT_TRAIN_IMAGES=$(find ../train_data -type f | wc -l)
COUNT_ALL_IMAGES=$(find ../raw_data -type f | wc -l)
echo "validation images: "$COUNT_VALIDATION_IMAGES", "$(( COUNT_VALIDATION_IMAGES * 100 / COUNT_ALL_IMAGES))"%" 
echo "train images: "$COUNT_TRAIN_IMAGES", "$(( COUNT_TRAIN_IMAGES * 100 / COUNT_ALL_IMAGES))"%" 
echo "all images: "$(find ../raw_data -type f | wc -l) 

