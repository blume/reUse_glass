#/bin/bash


#Declare Variables
#.../clean/cap/label/image
#../ *    /*  /*    /*
IMAGE_FORMAT='jpeg'
IMAGE_PATH='../raw_data/all/passata/*/*/*/*'
SIMILAR_PATH='../raw_data/all/similar/*/*'
RANDOM_PATH='../raw_data/all/random/*'
TRAIN_PATH='../train_data/'
VAL_PATH='../validation_data/'
POSITIV_TRAIN_DATA_DIR='passata'
NEGATIV_TRAIN_DATA_DIR='nopassata'
TRAIN_DATA_STRUCTURE='{'$POSITIV_TRAIN_DATA_DIR',nopassata}'
TRAIN_DATA_SUBDIRS=('passata' 'nopassata')

# remove old data structure(delete all files and symlinks in train data)
rm -r ../train_data/*
rm -r ../validation_data/*

# create new training subdirs
for dir in ${TRAIN_DATA_SUBDIRS[@]}
do
	mkdir -p $TRAIN_PATH''$dir
	mkdir -p $VAL_PATH''$dir
done

# Copy all positiv images to traindata_passata
echo $IMAGE_PATH
I=0
for file in $IMAGE_PATH$IMAGE_FORMAT
do
	# evry tentht image (randomized) is for validation
	if [ $((1 + RANDOM % 10)) != 10 ]; then
	    cp $file $TRAIN_PATH''$POSITIV_TRAIN_DATA_DIR'/'
	else 
	    cp $file $VAL_PATH''$POSITIV_TRAIN_DATA_DIR'/'
	fi
	let I++
done

# Copy all negativ images
I=0
for file in $RANDOM_PATH$IMAGE_FORMAT
do
	# evry tentht image (randomized) is for validation
	if [ $((1 + RANDOM % 10)) != 10 ]; then
	    cp $file $TRAIN_PATH''$NEGATIV_TRAIN_DATA_DIR'/' 
	else
	    cp $file $VAL_PATH''$NEGATIV_TRAIN_DATA_DIR'/' 
        fi
	let I++
done

I=0
for file in $SIMILAR_PATH$IMAGE_FORMAT
do
	# evry tentht image (randomized) is for validation
	if [ $((1 + RANDOM % 10)) != 10 ]; then
	    cp $file $TRAIN_PATH''$NEGATIV_TRAIN_DATA_DIR'/' 
	else
	    cp $file $VAL_PATH''$NEGATIV_TRAIN_DATA_DIR'/' 
        fi
	let I++
done
exit
