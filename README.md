# ReUse-Glass #
## The Idea ##
In Germany all glass containers which aren't in the refund system are melted by up to 1600°C, just to make the same glass containers again.
This project is about building an AI-bases system to detect containers of the same shape and size, therefore they can be sorted washed and reused.

## Overview ##
img 1                      | img 2
:-------------------------:|:-------------------------:
<img src="./readme_resources/pycam.jpg" width="100%"> | <img src="./readme_resources/make_photo.jpg" width="100%">




To simulate a reverse wending machine I use a raspberry pi with a camera (img 2) and make pictures out of the same distance with the same background but in different light (img 2), like it would be in a reverse wending machine. To reduce complexity and the effort of generating training data only passata (img 2) containers can be detect out of other glas containers, bottles and random objects.
![](./readme_resources/prototyp.png)
If a needed glass container (passata) is detected, the reverse wending machine sends a message to the web server, with the information what kind of container is detected and where.
Logistics partner can then check on where are how many containers are. And the plan their routes based on these informations.
## Build ##
### AI Training ###
As Model a pretrained MobileNetV2 is used.
For the use of transfer learning the biggest part of the Model kept frozen only the last layers of pooling and feature extraction and the classification layer will be trained. 
#### Data ####
The images to train the CNN (AI-Model) are divided into two types: passata and nopassata. Passata are the typical passata glasses like in image 1. Nopassata is everything else bottles, other glass containers and random things people could throw in reverse wending machines.
The dataset in this repo only has a few samples. I don't have a could storage to provide it for download. If you want to retrain the CNN you have to create your own.
##### Data structure ####
I gave the training data the following directory structure.
![data_structure.jpg](readme_resources/data_structure.jpg)
I used this directory structure to keep track of the different variations of images to get an balanced dataset.
I wrote two bash scripts to manage the data copy_data.sh and count.sh. Copy data copy the data of this directory structure in a train and a validation directory with a random split by 90/10.
count.sh is outputs the number of train, validation and overall images.
##### Data generation ###
If raspi, camera and the glass is setup like on img 1
Login to the raspberry pi and use the following command to make a picture:

raspistill -o - -t 300 | convert - -rotate -90 passata1_$(date +%d.%m.%Y-%T).jpeg

The number behind the passata is to identify images of the same glasses. The timestamp is used that no images have the same name.
Obvious hint: If you make a photo of a different object give it a different name

#### Install dependencies ####
1. Install python3.8 and pip
2. update pip: pip install --update pip
3. Install virtualenv with pip: pip install virtualenv
4. go training directory: cd cnn_training/src/
5. create virtual environment: python3 -m virtualenv --python=/path/to/python3.8 venv
6. activate venv: source venv/bin/activate
7. install dependencies: pip install -r requirements.txt

#### Run Training ####
1. go training directory: cd cnn_training/src/
2. activate venv: source venv/bin/activeate
3. python create_and_train_nn.py
Hint: It wouldn't make much sense train the cnn, if you don't use additional data. 

#### Training results ####
After two graphs are plotted on which you can decide how good the model performs.
![](./readme_resources/result.png)
Accuracy is calculated by (right classifications)/(all classifications).  The most important here is that the training accuracy don't raise while validation stagnates or drops. This would be a clear sign of oeveriffing.
### Reverse Wending Machine (Rasp pi) ###
#### Install ####
1. copy client folder to raspberry pi
2. Install python3.8 and pip
3. update pip: pip install --update pip
4. Install virtualenv with pip: pip install virtualenv
5. create virtual environment: python3 -m virtualenv --python=/usr bin/python3.8 venv
6. activate venv: source venv/bin/activeate
7. install dependencies: pip install -r requirements.txt
#### Run ####
1. activate venv: source venv/bin/activeate
2. python tflite_classify.py
3. raspistill -o - -t 300 | convert - -rotate -90 jpeg:- | python3 /path/to/tflite_classify.py

To test on your PC: cat /path/to/image | python3
/path/to/tflite_classify.py

Not so obvious hint: The Notwork is only trained with the same background and distance, if you use a different background or distance, chances are pretty high that it would fail.

Hints for using the web server: In line 53 of the tflite_classify script change localhost to the ip-adress where the web server is running and change your firewall configuration (open port 8080) (be careful with this).




### Webserver ###
#### Database ####
1. Install MariaDB and and a mysql client.
2. Create the database with the following command:

	create database db_name; 

3. And setup a user and password, therefore the Spring application don't have to login as root.

	create user 'username'@'%' identified by 'password';

4. Give the user the permission to manipulate the database.

	WARNING: Set stricter rules for production e.g. Drop and Create table 		shouldn't be allowed. 

	grant all on db_name.* to 'user_name’@'%';
5. Finish with: 
	
	flush privileges;
	
	exit;
#### Spring Boot Application ####
##### Build and run #####
1. Install and setup java 1.8
2. Insert user, database_name and password in the application.properties file (located in  server/reuseglass/src/main/resources/)

	WARING: The file is in the git, for safety it would be better to 		remove it from the repo, since you don't want to publish passwords or 		usernames.

3. run with:  server/reuseglass/./gradlew bootRun

##### Debug #####
Build Faild because the test contextLoads() failed:
Make sure your database is running and your application.properties file is correct. 
### Usage ###
The web server has multiple endpoints:

* Add a new Container

	HTTP-Post request at /reusegalss/add with attributes type, size, 		city, plz, and street.

	curl localhost:8080/reuseglass/add -d type=aufstrich -d size=720 -d 	city=Konstaz -d plz=74076 -d street='hauck-strasse 5'

* List all containers

	HTTP Get request at /reuseglass/all

	curl localhost:8080/reuseglass/all

* Filter by type and or city and or size

	HTTP Get request at /reuseglass/filter

	curl localhost:8080/reuseglass/filter?type=passata\&city=Konstanz

* Delete Multiple Containers by city and street and plz and type and size. (Specific containers in specific locations)

	HTTP Delete request at /reuseglass/delete

	curl -X ’DELETE’ localhost:8080/reuseglass/delete -d
	type=aufstrich -d size=720 -d city=Konstaz -d plz=74076 -d
	street=’hauck-strasse’ -d count=1
	
	Hint: Ignore count to delete all.



## Continuation ##
Since this was my my first AI project a lot could be improved but ...
After further research and talks to potential Stakeholders I discovered that the real problem behind reusing glass containers isn't the automated detection it's the space, work and intensive logistics why most producers don't do it when they don't have to.
Since I'm not into logistics and don't want to deep dive into it I didn't continued this project after building this first proof of concept prototype.
But anyway if you have any Ideas where to apply or even have a similar use case where this could be useful, feel free to contact me.