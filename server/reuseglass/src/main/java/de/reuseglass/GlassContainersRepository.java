package de.reuseglass;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete

public interface GlassContainersRepository extends CrudRepository<GlassContainers, Integer> {
	
	List<GlassContainers> findByCity(String city);
	List<GlassContainers> findByStreet(String street);
	List<GlassContainers> findByType(String type);
	List<GlassContainers> findByCityAndType(String city, String type);
	List<GlassContainers> findByCityAndStreet(String city, String street);
	List<GlassContainers> findByStreetAndType(String street, String type);
	List<GlassContainers> findByCityAndStreetAndType(String city, String street, String type);
	
	Long deleteByCityAndTypeAndStreet(String city, String type, String street);
}
