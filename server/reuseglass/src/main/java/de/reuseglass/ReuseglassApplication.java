package de.reuseglass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReuseglassApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReuseglassApplication.class, args);
	}

}
