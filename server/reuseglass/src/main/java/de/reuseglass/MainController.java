package de.reuseglass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping (path="/reuseglass")
public class MainController {
	@Autowired
	private GlassContainersRepository glassContainersRepository;
	
	@PostMapping(path="/add")
	public @ResponseBody String addNewContainer(@RequestParam String type,
												@RequestParam int size,
												@RequestParam String city,
												@RequestParam int plz,
												@RequestParam String street) {
		boolean exists = false;
		GlassContainers gc = new GlassContainers();
		gc.setType(type);
		gc.setSize(size);
		gc.setCount(1);
		gc.setCity(city);
		gc.setPlz(plz);
		gc.setStreet(street);
		glassContainersRepository.findAll();
		for (GlassContainers container : glassContainersRepository.findAll()) {
			if(gc.equals(container)) {
				container.increaseCount();
				glassContainersRepository.save(container);
				exists = true;
				break;
			}
		}
		if(! exists)
			glassContainersRepository.save(gc);
		
		return "Saved";
	}public MainController() {
		// TODO Auto-generated constructor stub
	}
	
	@Transactional
	@DeleteMapping(path="/delete") 
	public @ResponseBody String deleteContainers(@RequestParam String type,
												 @RequestParam int size,
												 @RequestParam String city,
												 @RequestParam int plz,
												 @RequestParam String street,
												 @RequestParam(required = false) Integer count){
		GlassContainers gc = new GlassContainers();
		gc.setType(type);
		gc.setSize(size);
		gc.setCount(1);
		gc.setCity(city);
		gc.setPlz(plz);
		gc.setStreet(street);
		
		GlassContainers gcr = null;
		for( GlassContainers glassContainer : glassContainersRepository.findAll()) {
			if(glassContainer.equals(gc)) {
				gcr = glassContainer;
			}
		};
		
		if(gcr == null)
			return "Not Found";
		if(count == null || count >= gcr.getCount())
			glassContainersRepository.deleteById(gcr.getId());
		else {
			gcr.setCount(gcr.getCount() - count);
			glassContainersRepository.save(gcr);
		}
		return "Deleted";
		
	}
	
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<GlassContainers> getAllGlassContainers(){
		return glassContainersRepository.findAll();
	}
	
	@GetMapping(path="/filter")
	public @ResponseBody Iterable<GlassContainers> getAllGlassContainersofCity(@RequestParam(required = false) String city,
																			   @RequestParam(required = false) String street,
																			   @RequestParam(required = false) String type){
		if(city != null && street != null && type !=null)
			return glassContainersRepository.findByCityAndStreetAndType(city, street, type);
		if(city != null && street != null)
			return glassContainersRepository.findByCityAndStreet(city, street);
		if(city != null && type != null)
			return glassContainersRepository.findByCityAndType(city, type);
		if(street != null && type != null)
			return glassContainersRepository.findByStreetAndType(street, type);
		if(city != null)
			return glassContainersRepository.findByCity(city);
		if(street != null)
			return glassContainersRepository.findByStreet(street);
		if(type != null)
			return glassContainersRepository.findByType(type);
		else
			return glassContainersRepository.findAll();
		
	}
}
