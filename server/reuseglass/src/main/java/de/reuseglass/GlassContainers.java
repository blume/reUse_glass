package de.reuseglass;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javassist.expr.Instanceof;

@Entity //tells hibernate to make a table out of this class
public class GlassContainers {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String type;
	private String city;
	private String street;
	
	private int size;
	private int count;
	private int plz;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getPlz() {
		return plz;
	}
	public void setPlz(int plz) {
		this.plz = plz;
	}
	
	public void increaseCount() {
		count += 1;
	}
	
	@Override
	/* compare all attributs but count*/
	public boolean equals(Object obj) {
		if( ! (obj instanceof GlassContainers))
			return false;
		if(obj == this)
			return true;
		GlassContainers gc = (GlassContainers) obj;
		
		return 	type.equals(gc.type)
				&& city.equals(gc.city)
				&& plz == gc.plz
				&& street.equals(gc.street);
	}
	
}
